<%-- Creare un link in una jsp con taglib --%>

<%- LINK CON TAGLIB -%>

<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>

<%- LINK CON TAGLIB con un parametro alla stessa portlet -%>

<liferay-portlet:renderURL var="plineSelectorURL">
	<liferay-portlet:param name="codice" value="AF12"/>
</liferay-portlet:renderURL>

<%- LINK CON T4GLIB a portlet differente -%>

<liferay-portlet:renderURL  varImpl="scopriLaGammaUrl" plid="<%= productsPLID %>" portletName="11_WAR_productsportlet" >
	<liferay-portlet:param name="pline" value="LVSGL"></liferay-portlet:param>
	<liferay-portlet:param name="referer" value="promo-dw"></liferay-portlet:param>
</liferay-portlet:renderURL>

<%- LINK CON T4GLIB a una jsp della stessa portlet-%>

<liferay-portlet:renderURL>
	<liferay-portlet:param name="jspPage" value="/promo/html/it/dw/form.jsp"></liferay-portlet:param>
</liferay-portlet:renderURL>