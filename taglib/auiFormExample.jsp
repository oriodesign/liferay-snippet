<!-- AUI FORM EXAMPLE -->

<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>


<aui:form action="<%= sendMessageActionUrl %>" method="post">
  <aui:input name="email" label="Email *">
     <aui:validator name="required" errorMessage="Campo obbligatorio" />
     <aui:validator name="email" errorMessage="Formato email non valido"/>
  </aui:input>
</aui:form>
