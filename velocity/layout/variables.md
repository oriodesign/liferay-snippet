## Variabili disponibili in un layout template

$processor          com.liferay.portal.layoutconfiguration.util.velocity.TemplateProcessor
$request            javax.servlet.http.HttpServletRequest	
$themeDisplay       com.liferay.portal.theme.ThemeDisplay	
$company            com.liferay.portal.model.Company	
$user               com.liferay.portal.model.User	
$realUser           com.liferay.portal.model.User	
$layout             com.liferay.portal.model.Layout	
$layouts            java.util.List<com.liferay.portal.model.Layout>
$plid               java.lang.Long
$layoutTypePortlet  com.liferay.portal.model.LayoutTypePortlet	
$portletGroupId     java.lang.Long
$locale             java.util.Locale
$timeZone           java.util.TimeZone
$theme              com.liferay.taglib.util.VelocityTaglib	
$colorScheme        com.liferay.portal.model.ColorScheme	
$portletDisplay     com.liferay.portal.theme.PortletDisplay	

