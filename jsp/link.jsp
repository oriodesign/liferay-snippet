<%- LINK senza t4glib -%>

long bookletPlid = LayoutLocalServiceUtil.getDefaultPlid(themeDisplay.getScopeGroupId(), false, ProductsPortletKeys.BOOKLET);
PortletURLFactoryUtil.create(renderRequest, ProductsPortletKeys.BOOKLET, bookletPlid, PortletRequest.RENDER_PHASE);
dmPortletURL.setParameter("query", ifp.getModelCode());

<%- IN VELOCITY friendly url -%>

#set($layoutLocalService = $serviceLocator.findService("com.liferay.portal.service.LayoutLocalService"))
#set( $homeSliderPortletId    = "1_WAR_sliderhomeportlet")
#set($homePagePlid = $layoutLocalService.getDefaultPlid($themeDisplay.getScopeGroupId(),false))
#set($homePageURL = $portalUtil.getLayoutFriendlyURL($layoutLocalService.getLayout($homePagePlid) , $themeDisplay))

<%- IN VELOCITY portlet url -%>

#set( $searchPortletId = "2_WAR_productsportlet")
#set($searchPortletPlid = $layoutLocalService.getDefaultPlid($themeDisplay.getScopeGroupId(),false, $searchPortletId))
#set($searchPortletURLImpl = $portletURLFactory.create($request, $searchPortletId, $searchPortletPlid, "RENDER_PHASE"))
$productsPortletURLImpl.setParameter("pline","LVTRC")
#set( $washerPageURL = $productsPortletURLImpl.toString())
